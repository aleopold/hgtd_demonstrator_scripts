from ROOT import TFile, TTree, TTimeStamp, TEnv, TDatime
import pandas as pd
from openpyxl import load_workbook
from datetime import datetime
import re
from array import array
import argparse
import uproot3
import os
from translateXLSX2ROOT import extractFilenameFromInput

def main(config):
    ##open rootfile
    input_xlsx_file = config.GetValue('input_xlsx_file', 'TestFile.xlsx')
    rootfile_name = extractFilenameFromInput(input_xlsx_file) + 'root'

    print("reading ", rootfile_name, "...")

    file = TFile.Open(rootfile_name, "READ")

    tree = file.Get('tree')

    date_time = TTimeStamp()
    tree.SetBranchAddress("date_time", date_time)

    arrays = {}
    for branch in tree.GetListOfBranches():
        branch_name = branch.GetName()
        if branch_name == "date_time":
            continue
        arrays[branch_name] = array('f',[0.])
        print(branch.GetName())
        tree.SetBranchAddress(branch_name, arrays[branch_name])

    ### LOOP OVER TREE
    print(tree.GetEntries(), 'entries in the tree')
    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        print(arrays['F2_101'])

    file.Close()

if __name__ == '__main__':
    ##parse the arguments from the command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="configXLS2ROOT.cfg", help="Name of the config file.")

    args = parser.parse_args()

    ## create the configuration object
    config = TEnv(args.config)

    main(config)
