from ROOT import TFile, TTree, TTimeStamp, TEnv, TDatime
import pandas as pd
from openpyxl import load_workbook
from datetime import datetime,timezone, timedelta
import re
from array import array
import argparse
import os

g_time_stamp_column_name = 'Scan Sweep Time (Sec)'

def readXLSXFileIntoDataFrame(path):
    """
    Reads the excel sheet into a pandas dataframe. The meta data at the begin of
    the file is skipped, searches for a specific string to define which lines
    to skip which is encoded in g_time_stamp_column_name.
    """
    ### NOTE this does not work, workaround found using openpyxl
    # df = pd.read_excel(path, engine='openpyxl')
    # df = pd.read_excel(path, sheet_name='Sheet1')
    ### NOTE workaround:
    # Load workbook
    wb = load_workbook(path)
    # Access to a worksheet named 'no_header'
    ws = wb['Sheet1']
    # Convert to DataFrame
    df = pd.DataFrame(ws.values)

    # Find label of the first row where the value is found (within column 0)
    row_label = (df.iloc[:, 0] == g_time_stamp_column_name).idxmax()
    # new_header = df.iloc[0] #grab the first row for the header

    ##set the header at this line
    df.columns = df.iloc[row_label]
    # Drop all rows above the set header row
    df = df.loc[row_label+1:, :]
    df = df.reset_index()
    df = df.drop(columns=['Scan Number', 'index'])

    ##drop columns with NaN values
    df = df.dropna(axis='rows')

    for col in df.columns:
        if col == g_time_stamp_column_name:
            df[col]= pd.to_datetime(df[col])
            continue
        df[col] = pd.to_numeric(df[col])

    df = df.set_index([g_time_stamp_column_name])

    return df

def getChannelNameTranslator(path):
    """
    The CSV files store values for channel numbers. To know which NTC or PT100
    is stored in which channel, a helper file is created which this function
    reads into a dictionary.
    After the keyword 'SENSORS:', things are written like:
    CH0 HEXT-1
    CH1 HEXT-2
    CH2 HTop-1
    CH3 HTop-2
    CH4 HMi-1
    ...
    """
    dict = {}
    ##open file
    read = False
    with open(path, "r") as info_file:
        ##find the keyword in the file and read all lines below
        for line in info_file:
            if read:
                line = line.strip()
                channel, name = line.split(' ')
                dict[channel] = name
            if line.startswith("SENSORS"):
                read = True
    return dict

def labviewDateParse(labview_time):
    from dateutil.relativedelta import relativedelta

    date = datetime.fromtimestamp(float(labview_time))
    ##FIXME check if this is true for your case!!
    ## subtract 2 hourse since this is UTC
    date = date - relativedelta(years=66, days=1, hours=0)
    return date

def readCSVfileIntoDataFrame(path):
    """
    Reads the stored CSV files. The timestamps are LabVIEW timestamps given as
    seconds since the epoch 01/01/1904 00:00:00.00 UTC. This needs to be corrected
    to be consistent with the other datasets.
    """
    if '*' in path:
        return readMultipleCSVfileIntoDataFrame(path)
    df = pd.read_csv(path, parse_dates=True, date_parser=labviewDateParse, index_col='Time')
    ##drop columns with NaN values
    df = df.dropna(axis='columns')
    ##drop columns with voltage readout, which have a 'Vout-CHXX' name
    cols = [c for c in df.columns if 'Vout' not in c]
    df = df[cols]
    ##convert string columns to numeric values
    for col in df.columns:
        if 'Time' in col:
            continue
        df[col] = pd.to_numeric(df[col])
    return df

def readMultipleCSVfileIntoDataFrame(path):
    """
    Reads the stored CSV files. The timestamps are LabVIEW timestamps given as
    seconds since the epoch 01/01/1904 00:00:00.00 UTC. This needs to be corrected
    to be consistent with the other datasets.
    """
    from glob import glob
    all_files = glob(path)

    df_list = []
    for filename in all_files:
        tmp_df = pd.read_csv(filename, index_col=None, header=0)
        df_list.append(tmp_df)

    df = pd.concat(df_list, axis=0, ignore_index=True)
    # df = pd.read_csv(path, parse_dates=True, date_parser=labviewDateParse, index_col='Time')
    ##drop columns with NaN values
    df = df.dropna(axis='columns')
    ##drop columns with voltage readout, which have a 'Vout-CHXX' name
    cols = [c for c in df.columns if 'Vout' not in c]
    df = df[cols]
    ##convert string columns to numeric values
    for col in df.columns:
        ##in the power files, the time column is called "Timestamp"
        if 'Time' in col:
            # df[col]= pd.to_datetime(df[col]) ## does not work with labview timestamps
            df[col] = df[col].transform(labviewDateParse)
            df = df.set_index([col])
            continue
        df[col] = pd.to_numeric(df[col])
    return df

def addColumnSumOfOtherColumns(df, column_list, new_column_name):
    """
    As in the case for the power, the total power is split into different channels
    and a total power column can thus be added to the dataframe.
    All the columns specified in column_list are added.
    """
    df[new_column_name] = df[column_list].sum(axis=1)

def addSuffixToColumnName(df, suffix):
    return df.rename(columns=lambda x: x+'_'+suffix if not 'Time' in x else x)

def removeRowsBasedOnCutoff(df, cutoff):
    for col in df.columns:
        if col == "Time":
            continue
        index = df[ df[col] < cutoff ].index
        # Delete these row indexes from dataFrame
        df.drop(index , inplace=True)
    return df


def resampleIntoTimePeriods(df, timeperiod):
    """
    Gives the possibility to average measurements in fixed periods
    """
    df = df.resample(timeperiod).mean()
    return df

def extractFilenameFromInput(input_xlsx_file):
    """
    Since the output file will carry the same name as the input file, the .xlsx
    or similar ending is removed to be replaced with .root later.
    Turns "/path/to/my/file/my_file.someending" to" my_file".
    """
    ##remove absolute or relative path part
    x = input_xlsx_file.split('/')[-1]
    ##fined name of ending
    ending = x.split('.')[-1]
    ##remove ending
    x = x.replace(ending, '')
    return x

def redefProbeNameForRootFile(name):
    """
    New: Changes names from "302-BT15_2" to "BT15_2" or from "PT100-BT7_2" to "BT7_2"
    Old: Changes temp sensor name from e.g. "102-F1" to "F1_102" to avoid issues in TTree.
    """
    if 'NTC' in name:
        return name
    if len(name.split('-')) != 2:
        return name
    a,b = name.split('-')
    # return f'{b}_{a}'
    return b



def writeToROOTFile(df, output_path, output_name):
    ##reset index to have time staps as column
    df = df.reset_index()
    # print(df.head())

    columns = df.columns
    # print(columns)

    ##set a branch for each column
    fullpath = os.path.join(output_path, output_name)
    file = TFile(fullpath, 'RECREATE')
    tree = TTree('tree', 'tree')
    arrays = {}
    for column in columns:
        # print(column)
        if column == g_time_stamp_column_name:
            ##TDatime
            arrays[column] = TDatime()
            ##TTimeStamp
            # arrays[column] = TTimeStamp()
            tree.Branch('date_time', arrays[column])
            # tree.Branch(column, arrays[column], column+'/F')
        else:
            arrays[column] = array('f',[0.])
            ##redefine the column name to avoid issues with ttree
            redef = redefProbeNameForRootFile(column)
            tree.Branch(redef, arrays[column], redef+'/F')

    for index, row in df.iterrows():
        for column in columns:
            if column == g_time_stamp_column_name:
                date_time = row[column]
                date = int(date_time.strftime("%Y%m%d"))
                time = int(date_time.strftime("%H%M%S"))
                # print(date, time)
                ##TDatime
                arrays[column].Set(date, time)
                ##TTimeStamp
                # arrays[column].Set(date, time, 0, False, TTimeStamp.GetZoneOffset()) ##for local
                # arrays[column].Set(date, time, 0) ##for UTC
            else:
                arrays[column][0] = row[column]

        tree.Fill()

    file.Write()
    file.Close()
    print(f"Done writing {fullpath}")

def extractTimberData(start_date, end_date, utc_time_shift=2):
    """
    Extracting the variables:
    CS1B_180_TT2H02.POSST  - HGTD outlet temperature - Rack 2 Card 4 ch 7 [°C]
    CS1B_180_TT1H01.POSST  - HGTD inlet temperature  - Rack 2 Card 4 ch 6 [°C]
    CS1B_180_PT2H02.POSST  - HGTD outlet pressure    - Rack 2 Card 5 ch 1 [bara]
    CS1B_180_PT1H01..POSST - HGTD inlet pressure     - Rack 2 Card 5 ch 0 [bara]
    CS1B_180_FT1H01.POSST  - HGTD flow measurement   - Rack 2 Card 4 ch 5 [g/s]

    Note: Timber stores time in UTC, UTC is 'CERN time - 2h' in summer and
    'CERN time - 1h' in winter.
    """
    import pytimber

    ##this need to be added, otherwise the returned array does not reach until the desired end date
    start_date = start_date - timedelta(hours=utc_time_shift)
    end_date = end_date + timedelta(hours=utc_time_shift)

    print(f"UTC start time: {start_date}, end time: {end_date} \n")

    vars = {'CS1B_180_TT2H02.POSST':'CO2Tout', 'CS1B_180_TT1H01.POSST':'CO2Tin', 'CS1B_180_PT2H02.POSST':'CO2Pout', 'CS1B_180_PT1H01.POSST':'CO2Pin', 'CS1B_180_FT1H01.POSST':'CO2flow'}

    ldb = pytimber.LoggingDB(source='nxcals')  # Backport API

    # print("start:", start_date, start_date.replace(tzinfo=timezone.utc).timestamp())
    data_tmp_series = {}
    for var in vars:
        ##the retrieved data is a dict, containing an array with the UTC time
        ##stamps and a second array holding the data
        ## d = {'varname':(array_time[], array_data[])}
        d = ldb.get(var, start_date, end_date)
        if not var in d:
            print(var, "not found")
            continue
        var_time = [datetime.utcfromtimestamp(x) + timedelta(hours=utc_time_shift) for x in d[var][0]]
        var_data = pd.Series(d[var][1], index=var_time)
        data_tmp_series[vars[var]] = var_data

    tmp_df = pd.DataFrame(data_tmp_series)

    tmp_df = tmp_df.interpolate('index')

    ##drop rows with NaN values
    tmp_df = tmp_df.dropna(axis='rows')

    return tmp_df

def writeHTML(path, df):
    csv_text = '''
    .mystyle {
        font-size: 11pt;
        font-family: Arial;
        border-collapse: collapse;
        border: 1px solid silver;

    }

    .mystyle td, th {
        padding: 5px;
    }

    .mystyle tr:nth-child(even) {
        background: #E0E0E0;
    }

    .mystyle tr:hover {
        background: silver;
        cursor: pointer;
    }
    '''

    html_string = '''
    <html>
      <head><title>HTML Pandas Dataframe with CSS</title></head>
      <link rel="stylesheet" type="text/css" href="df_style.css"/>
      <body>
        {table}
      </body>
    </html>.
    '''

    csv_file = os.path.join(path, 'df_style.css')
    html_file = os.path.join(path, 'index.php')

    html = df.to_html()

    styler = df.style.set_properties(**{'text-align': 'center'})
    html = styler.render()

    with open(html_file, 'w') as f:
        f.write(html_string.format(table=html))
    with open(csv_file, 'w') as f:
        f.write(csv_text)

def main():
    print("\nTranslating XLSX, CSV, txt files to ROOT format ... \n")
    ##parse the arguments from the command line
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default="configXLS2ROOT.cfg", help="Name of the config file.")

    args = parser.parse_args()



    ## create the configuration object
    config = TEnv(args.config)
    print(f"Reading configuration from {args.config} ... \n")

    DEBUG_MODE = config.GetValue('debug', False)

    input_path = config.GetValue('input_path', '.')

    ## PART 1: read the excel file into a dataframe
    input_xlsx_file = config.GetValue('input_xlsx_file', 'TestFile.xlsx')

    input = os.path.join(input_path, input_xlsx_file)

    print(f"Reading XLSX file {input_xlsx_file} ... \n")
    df_xlsx = readXLSXFileIntoDataFrame(input)
    ##something wrong with time, shift by 1 h
    keysight_time_shift = config.GetValue('keysight_time_shift', 0)
    df_xlsx = df_xlsx.shift(keysight_time_shift, freq='h')

    if DEBUG_MODE:
        print(df_xlsx.head())
    ##rename the columns to remove units in parenthesis
    df_xlsx = df_xlsx.rename(columns=lambda x: re.sub(r'\([^)]*\)', '', x))
    ##rename the columns to remove white space
    df_xlsx = df_xlsx.rename(columns=lambda x: x.replace(' ', ''))

    if DEBUG_MODE:
        print(df_xlsx.head())

    ## read the PT100 data
    input_pt100_file = config.GetValue('input_pt100_file', 'default')
    print(f"Reading CSV file {input_pt100_file} ... \n")
    df_pt100 = readCSVfileIntoDataFrame(os.path.join(input_path, input_pt100_file))
    df_pt100 = removeRowsBasedOnCutoff(df_pt100, config.GetValue('min_temperature_cutoff', 999.))

    if DEBUG_MODE:
        print(df_pt100.head())

    ## read the NTC data
    input_ntc_file = config.GetValue('input_ntc_file', 'default')
    print(f"Reading CSV file {input_ntc_file} ... \n")
    df_ntc = readCSVfileIntoDataFrame(os.path.join(input_path, input_ntc_file))
    df_ntc = removeRowsBasedOnCutoff(df_ntc, config.GetValue('min_temperature_cutoff', 999.))

    if DEBUG_MODE:
        print(df_ntc.head())

    ## read the Power data
    input_power_file1 = config.GetValue('input_power_file1', 'default')
    print(f"Reading CSV file {input_power_file1} ... \n")
    df_pow1 = readCSVfileIntoDataFrame(os.path.join(input_path, input_power_file1))

    ## add up the power columns to get a total power column
    # addColumnSumOfOtherColumns(df_pow1, [x for x in df_pow1.columns if not 'Time' in x], 'TotalPower')
    df_pow1 = addSuffixToColumnName(df_pow1, "HMP404")

    input_power_file2 = config.GetValue('input_power_file2', 'default')
    print(f"Reading CSV file {input_power_file2} ... \n")
    df_pow2 = readCSVfileIntoDataFrame(os.path.join(input_path, input_power_file2))

    ## add up the power columns to get a total power column
    # addColumnSumOfOtherColumns(df_pow2, [x for x in df_pow2.columns if not 'Time' in x], 'TotalPower')
    df_pow2 = addSuffixToColumnName(df_pow2, "PLQMD")

    power_column = df_pow1.columns+df_pow2.columns

    start_time = df_xlsx.index[0]
    end_time = df_xlsx.index[-1]

    print(f"Reading data from timber... \n")
    print(f"Start time: {start_time}, end time: {end_time} \n")

    ## read the timber data
    utc_time_shift = config.GetValue('utc_time_shift', 0)
    timber_data = extractTimberData(start_time, end_time, utc_time_shift)

    ## apply a resampling into specified intervals
    resampling_period = config.GetValue('resampling_period', '0').strip()
    if resampling_period != '0':
        timber_data = resampleIntoTimePeriods(timber_data, resampling_period)
        df_xlsx = resampleIntoTimePeriods(df_xlsx, resampling_period)
        df_pt100 = resampleIntoTimePeriods(df_pt100, resampling_period)
        df_ntc = resampleIntoTimePeriods(df_ntc, resampling_period)
        df_pow1 = resampleIntoTimePeriods(df_pow1, resampling_period)
        df_pow2 = resampleIntoTimePeriods(df_pow2, resampling_period)

    if DEBUG_MODE:
        print(timber_data.head())
        print(df_xlsx.head())
        print(df_pt100.head())
        print(df_ntc.head())
        print(df_pow1.head())
        print(df_pow2.head())

    combined_df = df_xlsx.join(timber_data)
    combined_df = combined_df.join(df_pt100)
    combined_df = combined_df.join(df_ntc)
    combined_df = combined_df.join(df_pow1)
    combined_df = combined_df.join(df_pow2)

    ##rename the channel name to the identifier
    input_info_file = config.GetValue('input_info_file', 'default')

    print(f"Reading text file {input_info_file} to translate channel names ... \n")
    channel_dict = getChannelNameTranslator(os.path.join(input_path, input_info_file))

    ##change channel names from e.g. 'T-CH19' to 'CH19'
    combined_df = combined_df.rename(columns=lambda x: x.replace('T-', ''))
    ##rename the columns to remove white space
    combined_df = combined_df.rename(columns=lambda x: x.replace(' ', ''))
    ## if the input_info_file defines an alias for a channel number, use it
    combined_df = combined_df.rename(columns=lambda x: channel_dict[x] if x in list(channel_dict.keys()) else x)
    ## remove units in parenthesis
    combined_df = combined_df.rename(columns=lambda x: re.sub(r'\([^)]*\)', '', x))

    addColumnSumOfOtherColumns(combined_df, [x for x in combined_df.columns if 'P-CH' in x], 'TotalPower')

    if DEBUG_MODE:
        print(combined_df.head())

    writeHTML('/afs/cern.ch/user/a/aleopold/www/hgtd/demonstrator_html', combined_df)

    output_path = config.GetValue('output_path', '.')
    output_name = config.GetValue('output_rootfile_name', 'default')

    writeToROOTFile(combined_df, output_path, output_name)

if __name__ == '__main__':
    main()
    # readCSVfileIntoDataFrame('/eos/atlas/atlascerngroupdisk/det-hgtd/demonstrator/coolingplate/version2run1/Run1*_power.csv')
