
##################################################
##################################################
### working on 2021/04/22
setupATLAS

lsetup "views LCG_97python3 x86_64-centos7-gcc8-opt"
VENV_PATH=./virt-env
unset PYTHONPATH
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt" ##lsetup "views LCG_97a x86_64-centos7-gcc8-opt"
python3 -m venv $VENV_PATH
source $VENV_PATH/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install numpy pandas
python3 -m pip install openpyxl
python3 -m pip install jinja2
python3 -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch  pytimber
