# Demonstrator analysis utilities

Collection of useful scripts to analyse the heater demonstrator data.


## Basic setup

A couple of non-standard libraries are needed. To install those, a virtual environment is set up in the prepared script.

For the first setup, do
```
$ source loadConfig.sh
```

### End virtual environment session

```
$ deactivate
```

### After new login

After the virt env is set, you can just activate with

```
$ source setup.sh
```


## Translate excel sheet to root

Paths to input and output files as well as additional settings are stored in a config file and read during runtime. An example is provided with the `configXLS2ROOT.cfg` file.

```
## set the name of the inputs you want to translate to ROOT
## data is stored under /eos/atlas/atlascerngroupdisk/det-hgtd/demonstrator/coolingplate
input_path: /eos/atlas/atlascerngroupdisk/det-hgtd/demonstrator/coolingplate/version2run1

input_xlsx_file: Run-1.xlsx
input_ntc_file: NTC-EXT-Data.csv
input_pt100_file: PT100-Data.csv
input_power_file1: HMP404_power_*.csv
input_power_file2: PLQMD_power_*.csv
input_info_file: Measurement-Info.txt

## shift time of Keysight timestamps to correct for misconfiguration
keysight_time_shift: 1

## shift applied to UTC timestamps to agree with CERN time
utc_time_shift: 2


## The measurements can be resampled in given time periods, an aveage of the measurements is set
## see https://pandas.pydata.org/docs/reference/api/pandas.Series.resample.html for syntax
## set to '0' if you don't want resampling
## WARNING: data is taken in different time steps by the different DAQ systems,
##          no resampling might cause instabilities when merging the data
resampling_period: 2min

## to filter out wrong readings, temperatures below a certain value can be ignored
min_temperature_cutoff: -50

output_rootfile_name: Version2Run1.root

## path where you want your ROOT file stored
output_path: /eos/user/a/aleopold/hgtd/demonstrator

debug: FALSE
```

The output rootfile will contain a single `TTree`, with one `TBranch` that stores the time stamps in `TDatime` format and an additional `TBranch` for each temperature probe containing the temperature measurements (in °C) at each time stamp as a `float` value, like

```
root [4] tree->Print()
******************************************************************************
*Tree    :tree      : tree                                                   *
*Entries :      241 : Total =           33557 bytes  File  Size =      18769 *
*        :          : Tree compression factor =   1.31                       *
******************************************************************************
*Br    0 :date_time : TDatime                                                *
*Entries :      241 : Total  Size=       2517 bytes  File Size  =        942 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   2.14     *
*............................................................................*
*Br    1 :F2_101    : F2_101/F                                               *
*Entries :      241 : Total  Size=       1526 bytes  File Size  =        840 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   1.23     *
*............................................................................*
*Br    2 :F3_102    : F3_102/F                                               *
*Entries :      241 : Total  Size=       1526 bytes  File Size  =        836 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   1.24     *
*............................................................................*
*Br    3 :F4_103    : F4_103/F                                               *
*Entries :      241 : Total  Size=       1526 bytes  File Size  =        826 *
*Baskets :        1 : Basket Size=      32000 bytes  Compression=   1.26     *
*............................................................................*
```

Run the rootification
```
$ python3 translateXLSX2ROOT.py -c configXLS2ROOT.cfg
```


## Data from timber database

The CO2 cooling plant (baby-demo) logs data while running, like temperatures and flow rates. These can be extracted via a python API (pytimber) and combined with the cooling plate data.

Since LCG setups might change (or be messed up) over time, a small test script is provided to check pytimber works as expected

```
$ python3 pytimber_test.py
```


## Notes

- Giving time stamps the same name (instead of Time, Timestamp) would simplify things, also maybe changing the labview timestamps to some string that can't be misinterpreted would be worthwhile
- When CSV file gets written, after restart, can the header be written again?
- The PT100 channel names could be the same everywhere (no "PT100" prefix)
- I would keep erroneous data in the raw files (can be treated in analysis)
- For the power, can they be stored in one file? Do we have a translation table for the channels?
