

void horizontalLine() {
  auto canvas = new TCanvas();
  auto axis_hist = new TH1F("", ";x;y", 1, 0, 1);
  axis_hist->SetAxisRange(-2, 2, "Y");
  axis_hist->Draw();
  canvas->Print("horizontalLine.pdf");
}
