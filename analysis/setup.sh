setupATLAS
##lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "views LCG_97a x86_64-centos7-gcc8-opt"

ALEX_LIB=/afs/cern.ch/work/a/aleopold/private/software/personal_lib
export LD_LIBRARY_PATH=${ALEX_LIB}/install/shared/lib/:${LD_LIBRARY_PATH}
export ROOT_INCLUDE_PATH=${ALEX_LIB}/inc:$ROOT_INCLUDE_PATH
