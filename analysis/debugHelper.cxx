
#include "sensorObjectAndReader.cxx"

void debugHelper() {
  RunData data =
      RunDataBuilder::retrieveRunData("globalConfig.cfg", "configV2R1.cfg");

  //print the sensor names:
  for (const auto& sensor : data.sensors) {
    cout << "Sensor name: " << sensor.name << '\n';
  }
}
