#include "Configurator.h"
#include "PlottingUtils.h"
#include <ctime>

typedef TDatime Date_t;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct TempSensor {
  TempSensor() = default;

  TString name;
  TString short_name;
  TTreeReaderValue<float>* reader_val;
  std::vector<float> data;
  TGraph* graph;
  std::vector<Date_t>* datetime;
  std::vector<float>* datetime_float;
  mutable std::vector<float> skipnan_time;
  mutable std::vector<float> skipnan_data;
  mutable std::vector<float> floor_nan_data;

private:
  mutable bool m_skipnan_is_cashed = false;
  mutable bool m_floornan_is_cashed = false;

  void cacheSkipNanVectors() const {
    size_t n_vals = data.size();
    skipnan_time.reserve(n_vals);
    skipnan_data.reserve(n_vals);
    for (size_t i = 0; i < n_vals; i++) {
      if (std::isnan(data.at(i))) {
        continue;
      }
      skipnan_time.push_back(datetime_float->at(i));
      skipnan_data.push_back(data.at(i));
    }
    m_skipnan_is_cashed = true;
  }

  void cachefloorNanVectors() const {
    size_t n_vals = data.size();
    floor_nan_data.reserve(n_vals);
    for (size_t i = 0; i < n_vals; i++) {
      floor_nan_data.push_back(std::isnan(data.at(i)) ? 0 : data.at(i));
    }
    m_floornan_is_cashed = true;
  }

public:
  void printStartAndEnd() {
    this->datetime->at(0).Print();
    this->datetime->at(this->datetime->size() - 1).Print();
  }

  TGraph* createValVsTimeGraph(bool skip_nan = false) {
    if (skip_nan) {
      if (not m_skipnan_is_cashed) {
        cacheSkipNanVectors();
      }
      auto* gr =
          new TGraph(skipnan_data.size(), &skipnan_time[0], &skipnan_data[0]);
      return gr;
    } else {
      size_t n_vals = data.size();
      auto* gr = new TGraph(n_vals, &(datetime_float->at(0)), &data[0]);
      return gr;
    }
  }

  TGraph* createValVsTimeGraphFloorNan() {
    if (not m_floornan_is_cashed) {
      cachefloorNanVectors();
    }
    return new TGraph(floor_nan_data.size(), &(datetime_float->at(0)),
                      &floor_nan_data[0]);
  }

  TGraph* createTempVsReadingGraph() const {
    size_t n_vals = data.size();
    std::vector<float> v(n_vals);
    std::iota(std::begin(v), std::end(v), 0);
    auto* gr = new TGraph(n_vals, &v[0], &data[0]);
    return gr;
  }

  TGraph* createTempVsReadingGraph(const TDatime& start_time,
                                   const TDatime& end_time) const {
    auto start_end = this->getStartEndPos(start_time, end_time);
    std::vector<float> vals(start_end.second - start_end.first);
    std::vector<float>::iterator vals_iter = vals.begin();
    for (size_t i = start_end.first; i < start_end.second; i++, vals_iter++) {
      *vals_iter = data.at(i);
    }
    std::vector<float> x_vec(start_end.second - start_end.first);
    std::iota(std::begin(x_vec), std::end(x_vec), 0);
    auto graph = new TGraph(vals.size(), &x_vec[0], &vals[0]);
    return graph;
  }

  TH1* createHisto(int start, int end) const {
    auto hist =
        new TH1F("hist" + name, ";Temperature [#circC];Number of readings", 100,
                 -40, -32);
    for (size_t i = start; i < end; i++) {
      hist->Fill(data.at(i));
    }
    return hist;
  }

  float getMean(int start, int end) const {
    return TMath::Mean(data.begin() + start, data.begin() + end);
  }

  float getRMS(int start, int end) const {
    return TMath::RMS(data.begin() + start, data.begin() + end);
  }

  float getMinVal(int start, int end) const {
    if (not m_skipnan_is_cashed) {
      cacheSkipNanVectors();
    }
    return *std::min_element(skipnan_data.begin() + start,
                             skipnan_data.begin() + end);
  }

  float getMinVal() const {
    if (not m_skipnan_is_cashed) {
      cacheSkipNanVectors();
    }
    return *std::min_element(skipnan_data.begin(), skipnan_data.end());
    // return *std::min_element(data.begin(), data.end());
  }

  float getMaxVal(int start, int end) const {
    if (not m_skipnan_is_cashed) {
      cacheSkipNanVectors();
    }
    return *std::max_element(skipnan_data.begin() + start,
                             skipnan_data.begin() + end);
  }

  float getMaxVal() const {
    if (not m_skipnan_is_cashed) {
      cacheSkipNanVectors();
    }
    return *std::max_element(skipnan_data.begin(), skipnan_data.end());
  }

  std::pair<int, int> getStartEndPos(const TDatime& start_time,
                                     const TDatime& end_time) const {
    int start = 0;
    int end = 0;
    for (const auto& time : *datetime) {
      if (time < start_time) {
        start++;
      }
      if (time < end_time) {
        end++;
      }
      if (time >= end_time) {
        break;
      }
    }
    return std::make_pair(start, end);
  }
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct RunData {
  AL::Configurator config;
  TString run_name;
  std::vector<TempSensor> sensors;
  std::vector<float> time;
  std::vector<Date_t> timedate;

  const TempSensor* getSensor(const TString& name) const {
    for (const TempSensor& sensor : sensors) {
      if (sensor.name == name or sensor.name.Contains(name)) {
        return &sensor;
      }
    }
    std::cout << "TempSensor: " << name << " does not exist, returning null\n";
    return nullptr;
  }
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

struct RunDataBuilder {

  static RunData retrieveRunData(TString global_config_name,
                                 TString config_name) {
    AL::Configurator global_config(global_config_name);

    AL::preparePlottingDir(global_config, "output_plot_path");

    AL::Configurator configurator(config_name);
    configurator.setValue("output_plot_path",
                          global_config.getStr("output_plot_path"));

    TFile* file =
        TFile::Open(configurator.getStr("input_root_file", "default"), "READ");

    RunData data = RunDataBuilder::readRunData(file, configurator);
    return data;
  }

  static RunData readRunData(TFile* file, AL::Configurator config) {
    RunData run_data;
    run_data.config = config;
    run_data.run_name = run_data.config.getStr("run_name", "default");
    TTree* tree = (TTree*)file->Get("tree");

    TObjArray* branch_list = tree->GetListOfBranches();
    TIter obj_iter(branch_list);
    while (TObject* obj = obj_iter()) {
      TString name(obj->GetName());
      // cout << "[RunData::readRunData] name: " << name << '\n';
      if (name == "date_time") {
        continue;
      }
      TempSensor sensor;
      sensor.name = name;
      // can be of form BT3_4_PT100 or BT15_3_303
      if (name.Contains('_')) {
        sensor.short_name = name.Remove(name.First('_'));
      } else {
        sensor.short_name = name;
      }
      run_data.sensors.push_back(sensor);
    }
    tree = nullptr;
    TTreeReader tree_reader("tree", file);
    int n = tree_reader.GetEntries();

    TTreeReaderValue<Date_t> date_time(tree_reader, "date_time");

    for (auto& sensor : run_data.sensors) {
      auto heap_reader = new TTreeReaderValue<float>(tree_reader, sensor.name);
      sensor.reader_val = heap_reader;
      sensor.data.reserve(n);
    }
    run_data.time.reserve(n);
    run_data.timedate.reserve(n);
    while (tree_reader.Next()) {
      run_data.time.push_back((*date_time).Convert());
      run_data.timedate.push_back((*date_time));
      for (auto& sensor : run_data.sensors) {
        float val = **(sensor.reader_val);
        // if (sensor.name == "CH1") {
        //   cout << "Value: " << val << '\n';
        // }
        sensor.data.push_back(val);
      }
    }

    // create the TGraph for each and set the datetime vector
    for (auto& sensor : run_data.sensors) {
      auto* gr = new TGraph(n, &run_data.time[0], &sensor.data[0]);
      sensor.graph = gr;
      sensor.datetime = &run_data.timedate;
      sensor.datetime_float = &run_data.time;
      delete sensor.reader_val; // also this is no longer needed
    }
    return run_data;
  }
};
