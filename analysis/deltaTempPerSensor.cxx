#include "Configurator.h"
#include "MathHelpers.h"
#include "PlottingStyle.h"
#include "PlottingUtils.h"
#include "TError.h"

#include "sensorObjectAndReader.cxx"

// gErrorIgnoreLevel = kWarning;

void plotDiffGraph(const TempSensor* sensor, TGraph* graph,
                   const AL::Configurator& config, float mean, float median,
                   float rms);

void plotSide(const std::vector<RunData>& run_data_vec,
              const vector<TString>& sensors) {

  // F: row 2, 6, 10, 14
  // B: row 3, 7, 11, 15
  vector<int> rows = {1, 3, 5};

  auto canvas = new TCanvas();
  canvas->SetGridx();
  size_t n_probes = sensors.size();
  auto axis_hist = new TH1F(
      "axis_hist", ";Probe identifier; #DeltaT(probe, inlet(CPD)) [#circC]",
      n_probes, 0, n_probes);
  for (int bin = 1; bin < axis_hist->GetNbinsX() + 1; bin++) {
    axis_hist->GetXaxis()->SetBinLabel(bin, sensors.at(bin - 1));
  }

  axis_hist->Draw();
  axis_hist->SetAxisRange(-3, 15.0, "Y");

  auto legend = new TLegend(0.65, 0.9, 0.85, 0.70);
  legend->SetTextFont(42);
  // legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.05);

  float offset = 1. / ((float)run_data_vec.size() + 1);
  float offset_step = offset;
  // Color_t color = 6;
  std::vector<Color_t> cols = {30, 40, 42, 46, 38};
  int col_i = 0;
  int marker_style = 20;
  // for each dataset
  for (auto& run_data : run_data_vec) {
    auto graph_err = new TGraphErrors();
    graph_err->SetName(run_data.run_name);
    // get the reference sensor
    const TempSensor* ref_sens = run_data.getSensor("CPD");
    auto start_end = ref_sens->getStartEndPos(
        TDatime(run_data.config.getStr("time_start", "")),
        TDatime(run_data.config.getStr("time_end", "")));
    cout << "run " << run_data.run_name << ": start " << start_end.first
         << " end " << start_end.second << '\n';
    cout << "offset " << offset << ": color " << col_i << " marker_style "
         << marker_style << '\n';
    // for each sensor
    // for (int sens_i = 1; sens_i <= axis_hist->GetNbinsX(); sens_i++) {
    for (int sens_i = 0; sens_i < n_probes; sens_i++) {
      // for (auto sensor : run_data.sensors) {
      const TempSensor* sensor = run_data.getSensor(sensors.at(sens_i));
      // cout << sensor->name << '\n';
      // get the difference
      std::vector<float> x_vec(start_end.second - start_end.first);
      std::iota(std::begin(x_vec), std::end(x_vec), 0);
      std::vector<float> diff_vals(start_end.second - start_end.first);
      std::vector<float>::iterator diff_vals_it = diff_vals.begin();
      for (size_t i = start_end.first; i < start_end.second;
           i++, diff_vals_it++) {
        // cout << "diff val: " << sensor.data.at(i) - ref_sens->data.at(i)
        // << '\n';
        *diff_vals_it = sensor->data.at(i) - ref_sens->data.at(i);
      }
      // cout << "size: " << diff_vals.size() << '\n';
      // cout << "diff_vals.at(10): " << diff_vals.at(10) << '\n';
      // cout << "diff_vals.at(diff_vals.size()-1): "
      //      << diff_vals.at(diff_vals.size()-1) << '\n';
      // calculate mean and RMS of the difference
      float mean = TMath::Mean(diff_vals.begin(), diff_vals.end());
      float median = AL::MathHelpers::median<float>(diff_vals);
      float rms = TMath::RMS(diff_vals.begin(), diff_vals.end());

      // cout << "run: " << run_data.run_name << '\n'
      //      << "sensor " << sensor->name << '\n'
      //      << "mean " << mean << '\n'
      // << "rms " << rms << '\n';

      // create the diff graph and plot it
      auto diff_graph = new TGraph(x_vec.size(), &x_vec[0], &diff_vals[0]);
      plotDiffGraph(sensor, diff_graph, run_data.config, mean, median, rms);

      graph_err->SetPoint(sens_i, sens_i + offset, mean);
      // graph_err->SetPointError(sens_i - 1, 0, rms); //remove to not give
      // false impression about validity of the uncertainty
      graph_err->SetPointError(sens_i, 0, 0);

      // cout << "run: " << run_data.run_name << " , sensor: " << sensor.name
      //      << ", diff mean: " << mean << ", rms: " << rms << '\n';

      graph_err->SetMarkerColor(cols.at(col_i));
      graph_err->SetMarkerStyle(marker_style);
      graph_err->SetMarkerSize(1);
      canvas->cd();
      graph_err->Draw("same P");

      // get name from the last
      if (sens_i == n_probes - 1) {
        legend->AddEntry(graph_err, graph_err->GetName(), "p");
      }
    } // LOOP over TempSensor
    // break;
    offset += offset_step;
    col_i++;
    marker_style++;
  } // LOOP over RunData

  legend->Draw("same");

  auto config = run_data_vec.at(0).config;

  TString plot_path = config.getStr("output_plot_path", "default");
  TString plot_name = Form("%s/plotDiffGraph_plotSide_%s.pdf", plot_path.Data(),
                           sensors.at(0).Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// void plotGraphWithReference(TempSensor* sensor, TEnv* config) {
//
// }
void plotDiffGraph(const TempSensor* sensor, TGraph* graph,
                   const AL::Configurator& config, float mean, float median,
                   float rms) {
  // void plotDiffGraph(TempSensor* sensor, TGraph* graph, AL::Configurator*
  // config,
  //                    float mean, float median, float rms) {

  double min = graph->GetHistogram()->GetMinimum();
  double max = graph->GetHistogram()->GetMaximum();

  auto canvas = new TCanvas();
  graph->GetXaxis()->SetTitle("Measurement");
  graph->GetYaxis()->SetTitle("#DeltaT(probe, inlet) [#circC]");
  graph->Draw("APL");

  double start_x = graph->GetPointX(1);
  double end_x = graph->GetPointX(graph->GetN() - 1);

  // cout << min << " " << max << " " << start_x << " " << end_x << '\n';

  TLine line_max(start_x, max, end_x, max);
  line_max.SetLineColor(kGray);
  line_max.SetLineStyle(3);
  line_max.Draw("same");

  TLine line_min(start_x, min, end_x, min);
  line_min.SetLineColor(kGray);
  line_min.SetLineStyle(3);
  line_min.Draw("same");

  AL::myText(0.16, 0.96, kBlack,
             Form("Probe Idenfier: %s", sensor->name.Data()), 0.035, 0.);
  AL::myText(0.4, 0.96, kBlack,
             Form("Run: %s", config.getStr("run_name", "default").Data()),
             0.035, 0.);

  AL::myText(0.2, 0.85, kBlack, Form("Mean #DeltaT: %.3f #circC", mean), 0.04,
             0.);
  AL::myText(0.2, 0.80, kBlack, Form("Median #DeltaT: %.3f #circC", median),
             0.04, 0.);
  AL::myText(0.2, 0.75, kBlack, Form("RMS #DeltaT:  %.3f#circC", rms), 0.04,
             0.);

  TString plot_path = config.getStr("output_plot_path", "default");
  TString plot_name =
      Form("%s/plotDiffGraph_%s%s.pdf", plot_path.Data(),
           config.getStr("run_name", "default").Data(), sensor->name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void deltaTempPerSensor() {
  gErrorIgnoreLevel = kWarning;

  RunData data_v2r1 =
      RunDataBuilder::retrieveRunData("globalConfig.cfg", "configV2R1.cfg");

  AL::setAtlasStyle();

  // cout << data_tial.getSensor("F1")->name << '\n';
  // return;

  // F: row 2, 6, 10, 14
  // B: row 3, 7, 11, 15
  vector<int> front_columns = {2, 6, 10, 14};
  vector<int> back_columns = {3, 7, 11, 15};
  vector<int> rows = {1, 3, 5};

  // produce the list of sensors before
  vector<TString> front_sensors;
  for (const auto& column : front_columns) {
    for (const auto& row : rows) {
      front_sensors.push_back(Form("FT%i_%i", column, row));
    }
  }
  vector<TString> back_sensors;
  for (const auto& column : back_columns) {
    for (const auto& row : rows) {
      back_sensors.push_back(Form("BT%i_%i", column, row));
    }
  }

  plotSide({data_v2r1}, front_sensors);
  plotSide({data_v2r1}, back_sensors);
}
