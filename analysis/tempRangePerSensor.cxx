#include "AtlasStyle.cxx"
#include "AtlasUtils.cxx"
#include "TError.h"
#include "helperFunctions.cxx"
#include "sensorObjectAndReader.cxx"

// gErrorIgnoreLevel = kWarning;

void plotDiffGraph(TempSensor* sensor, TGraph* graph, TEnv* config, float mean,
                   float rms);

void plotSide(std::vector<RunData*> run_data_vec, int n_probes,
              TString id_template) {

  auto canvas = new TCanvas();
  canvas->SetGridx();
  auto axis_hist = new TH1F(
      "axis_hist", ";Probe identifier; #DeltaT(probe, inlet(CPD)) [#circC]",
      n_probes, 0, n_probes);
  for (int bin = 1; bin < axis_hist->GetNbinsX() + 1; bin++) {
    axis_hist->GetXaxis()->SetBinLabel(bin, Form(id_template, bin));
  }
  axis_hist->Draw();
  axis_hist->SetAxisRange(-3, 15.0, "Y");

  auto legend = new TLegend(0.65, 0.9, 0.85, 0.70);
  legend->SetTextFont(42);
  // legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.05);

  float offset = 1. / ((float)run_data_vec.size() + 1);
  float offset_step = offset;
  // Color_t color = 6;
  std::vector<Color_t> cols = {30, 40, 42, 46, 38};
  int col_i = 0;
  int marker_style = 20;
  // vector<TGraphErrors*> diff_graphs(run_data_vec.size());
  // for each dataset
  for (auto& run_data : run_data_vec) {
    // auto graph_err = new TGraphErrors();
    auto graph_err = new TGraphMultiErrors(n_probes, 2);
    graph_err->SetName(run_data->run_name);
    // get the reference sensor
    TempSensor* ref_sens = run_data->getSensor("CPD");
    auto start_end = ref_sens->getStartEndPos(
        TDatime(run_data->config->GetValue("time_start", "")),
        TDatime(run_data->config->GetValue("time_end", "")));
    cout << "run " << run_data->run_name << ": start " << start_end.first
         << " end " << start_end.second << '\n';
    cout << "offset " << offset << ": color " << col_i << " marker_style "
         << marker_style << '\n';
    // for each sensor
    for (int sens_i = 1; sens_i <= axis_hist->GetNbinsX(); sens_i++) {
      // for (auto sensor : run_data->sensors) {
      TempSensor* sensor = run_data->getSensor(Form(id_template, sens_i));
      // cout << sensor->name << '\n';
      // get the difference
      std::vector<float> x_vec(start_end.second - start_end.first);
      std::iota(std::begin(x_vec), std::end(x_vec), 0);
      std::vector<float> diff_vals(start_end.second - start_end.first);
      std::vector<float>::iterator diff_vals_it = diff_vals.begin();
      for (size_t i = start_end.first; i < start_end.second;
           i++, diff_vals_it++) {
        // cout << "diff val: " << sensor.data.at(i) - ref_sens->data.at(i)
        // << '\n';
        *diff_vals_it = sensor->data.at(i) - ref_sens->data.at(i);
      }
      // cout << "size: " << diff_vals.size() << '\n';
      // cout << "diff_vals.at(10): " << diff_vals.at(10) << '\n';
      // cout << "diff_vals.at(diff_vals.size()-1): "
      //      << diff_vals.at(diff_vals.size()-1) << '\n';
      // calculate mean and RMS of the difference
      float mean = TMath::Mean(diff_vals.begin(), diff_vals.end());
      float rms = TMath::RMS(diff_vals.begin(), diff_vals.end());

      float max_diff =
          fabs(mean - *std::max_element(diff_vals.begin(), diff_vals.end()));
      float min_diff =
          fabs(mean - *std::min_element(diff_vals.begin(), diff_vals.end()));

      // cout << "run: " << run_data->run_name << '\n'
      //      << "sensor " << sensor->name << '\n'
      //      << "mean " << mean << '\n'
      // << "rms " << rms << '\n';

      // create the diff graph and plot it
      auto diff_graph = new TGraph(x_vec.size(), &x_vec[0], &diff_vals[0]);

      graph_err->SetPoint(sens_i - 1, sens_i - 1 + offset, mean);
      // graph_err->SetPointError(sens_i - 1, 0, rms);
      // graph_err->SetMarkerColor(cols.at(col_i));
      // graph_err->SetMarkerStyle(marker_style);
      // graph_err->SetMarkerSize(1);
      // canvas->cd();
      // graph_err->Draw("same P");
      graph_err->SetPointEX(sens_i - 1, 0., 0.);
      graph_err->SetPointEY(sens_i - 1, 0, rms, rms);
      graph_err->SetPointEY(sens_i - 1, 1, min_diff, max_diff);

      if (sens_i == 8) {
        legend->AddEntry(graph_err, graph_err->GetName(), "p");
      }
    } // LOOP over TempSensor
    graph_err->SetMarkerColor(cols.at(col_i));
    graph_err->SetMarkerStyle(marker_style);
    graph_err->SetMarkerSize(1);
    graph_err->GetAttLine(0)->SetLineColor(kRed);
    graph_err->GetAttLine(1)->SetLineColor(kBlue);
    graph_err->GetAttFill(1)->SetFillStyle(0);
    canvas->cd();
    // graph_err->Draw("same P");
    graph_err->Draw("same PS ; Z ; 5 s=0.5");
    // break;
    offset += offset_step;
    col_i++;
    marker_style++;
  } // LOOP over RunData

  legend->Draw("same");

  TEnv* config = run_data_vec.at(0)->config;

  TString plot_path = config->GetValue("output_plot_path", "default");
  TString plot_name = Form("%s/plotDiffGraph_plotSide_%s.pdf", plot_path.Data(),
                           id_template.ReplaceAll("%", "").Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void tempRangePerSensor() {
  gErrorIgnoreLevel = kWarning;

  AL::Configurator global_config("globalConfig.cfg");

  cout << "Load global config \n";

  AL::Configurator config_steel("configSteelPlotting.cfg");
  config_steel.setValue("output_plot_path",
                        global_config.getStr("output_plot_path"));

  AL::Configurator config_ti("configTiPlotting.cfg");
  config_ti.setValue("output_plot_path",
                     global_config.getStr("output_plot_path"));
  AL::Configurator config_ti2("configTi2Plotting.cfg");
  config_ti2.setValue("output_plot_path",
                      global_config.getStr("output_plot_path"));
  AL::Configurator config_tial("configTiAlPlotting.cfg");
  config_tial.setValue("output_plot_path",
                       global_config.getStr("output_plot_path"));
  AL::Configurator config_tial2("configTiAl2Plotting.cfg");
  config_tial2.setValue("output_plot_path",
                        global_config.getStr("output_plot_path"));

  cout << "Done with Configurators \n";

  TFile* file_steel =
      TFile::Open(config_steel.getStr("input_root_file", "default"), "READ");
  TFile* file_ti =
      TFile::Open(config_ti.getStr("input_root_file", "default"), "READ");
  TFile* file_ti2 =
      TFile::Open(config_ti2.getStr("input_root_file", "default"), "READ");
  TFile* file_tial =
      TFile::Open(config_tial.getStr("input_root_file", "default"), "READ");
  TFile* file_tial2 =
      TFile::Open(config_tial2.getStr("input_root_file", "default"), "READ");

  cout << "Done opening the rootfiles \n";

  AL::preparePlottingDir(config_steel, "output_plot_path");

  RunData data_steel = RunDataBuilder::readRunData(file_steel, &config_steel);
  RunData data_ti = RunDataBuilder::readRunData(file_ti, &config_ti);
  RunData data_ti2 = RunDataBuilder::readRunData(file_ti2, &config_ti2);
  RunData data_tial = RunDataBuilder::readRunData(file_tial, &config_tial);
  RunData data_tial2 = RunDataBuilder::readRunData(file_tial2, &config_tial2);

  SetAtlasStyle();

  // cout << data_tial.getSensor("F1")->name << '\n';
  // return;

  plotSide({&data_steel, &data_ti, &data_ti2, &data_tial, &data_tial2}, 8,
           "F%i");
  plotSide({&data_steel, &data_ti, &data_ti2, &data_tial, &data_tial2}, 10,
           "B%i");
}
