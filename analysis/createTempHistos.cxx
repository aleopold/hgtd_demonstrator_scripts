#include "AtlasStyle.cxx"
#include "AtlasUtils.cxx"
#include "helperFunctions.cxx"
#include "sensorObjectAndReader.cxx"

void createTempHistos(TString config_name = "configPlotting.cfg") {

  TEnv config = TEnv(config_name);

  Helper::preparePlottingDir(config, "output_plot_path");

  SetAtlasStyle();

  TString rootfile_name = config.GetValue("input_root_file", "default");
  TFile* file = TFile::Open(rootfile_name, "READ");
  //
  // RunDataBuilder builder;
  // builder.readRunData(file);
  //
  // RunData data = builder.run_data;

  RunData data = RunDataBuilder::readRunData(file, nullptr);

  cout << data.getSensor("F5")->short_name << '\n';

  TDatime start(config.GetValue("time_start", ""));
  TDatime end(config.GetValue("time_end", ""));

  cout << start.AsString() << '\n';

  for (auto& sensor : data.sensors) {

    std::pair<int, int> timelimit = sensor.getStartEndPos(start, end);
    auto hist = sensor.createHisto(timelimit.first, timelimit.second);
    auto canvas = new TCanvas();
    hist->Draw();
    hist->SetAxisRange(0., hist->GetMaximum()*1.3, "Y");
    atlas::myText(0.2, 0.88, kBlack, "Setup:", 0.04, 0.);
    atlas::myText(0.28, 0.88, kBlack, config.GetValue("setup_info", "default"),
                  0.04, 0.);
    TString selected_period = Form("Selected time range: [%s, %s]",
                                   config.GetValue("time_start", "default"),
                                   config.GetValue("time_end", "default"));
    atlas::myText(0.2, 0.84, kBlack, selected_period.Data(), 0.02,
                  0.);
    atlas::myText(0.2, 0.79, kBlack, "Temp Probe: " + sensor.short_name, 0.04,
                  0.);

    TString plot_path = config.GetValue("output_plot_path", "default");
    TString plot_name = Form("%s/createTempHistos_%s.pdf", plot_path.Data(),
                             sensor.name.Data());
    canvas->Print(plot_name);
    plot_name.ReplaceAll(".pdf", ".png");
    canvas->Print(plot_name);
    // break;
  }
}
