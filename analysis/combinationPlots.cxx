#include "Configurator.h"
#include "PlottingStyle.h"
#include "PlottingUtils.h"
#include "TError.h"

#include "sensorObjectAndReader.cxx"

void updateMinMaxVals(float& min, float& max, const TempSensor& sensor) {
  if (sensor.getMinVal() < min)
    min = sensor.getMinVal();
  if (sensor.getMaxVal() > max)
    max = sensor.getMaxVal();
}

void plotAllPT100AndCO2Flow(RunData& data) {
  // get graph for the front and back PT100s and get the CO2 flow graph
  vector<TGraph*> front_graphs;
  vector<TGraph*> back_graphs;
  TGraph* co2_flow_graph = nullptr;

  float min_temp = 0;
  float max_temp = 0;
  float min_co2 = 0;
  float max_co2 = 0;

  for (auto& sensor : data.sensors) {
    // create the graph
    auto* graph = sensor.createValVsTimeGraph(true);
    if (sensor.name.Contains("BT")) {
      back_graphs.push_back(graph);
      updateMinMaxVals(min_temp, max_temp, sensor);
    } else if (sensor.name.Contains("FT")) {
      front_graphs.push_back(graph);
      updateMinMaxVals(min_temp, max_temp, sensor);
    } else if (sensor.name == "CO2flow") {
      co2_flow_graph = graph;
      updateMinMaxVals(min_co2, max_co2, sensor);
    } else {
      delete graph;
    }
  }

  // setup the canvas
  AL::StyleSetter setter;
  setter.paperx = 100;
  setter.papery = 20;
  setter.top_margin = 0.05;
  setter.right_margin = 0.13;
  setter.bottom_margin = 0.16;
  setter.left_margin = 0.13;
  setter.title_x_offset = 1.4;
  setter.title_y_offset = 1.1;
  setter.label_x_offset = 0.03;
  setter.label_y_offset = 0.01;
  setter.font_size = 0.05;
  setter.set_ticks_x = 0;
  setter.set_ticks_y = 0;

  setVariableStyle(setter);

  // auto canvas = new TCanvas(); // is 700x500 by default
  auto canvas = new TCanvas("canvas", "canvas", 900, 500);

  auto axis_hist = new TH1F("", ";;Temperature [#circC]", 1, 0, 1);
  axis_hist->SetAxisRange(min_temp - 2, max_temp + 2, "Y");
  auto xaxis = axis_hist->GetXaxis();
  xaxis->SetTimeDisplay(1);
  xaxis->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  // xaxis->SetTimeFormat("%H:%M");
  xaxis->SetTimeOffset(0);
  // xaxis->SetNdivisions(-219);
  xaxis->SetLimits(data.time.at(0), data.time.at(data.time.size() - 1));
  // xaxis->CenterLabels();
  axis_hist->Draw("*");

  // draw the graphs
  for (const auto& graph : front_graphs) {
    graph->SetLineColor(kMagenta - 4);
    graph->Draw("same L");
  }
  for (const auto& graph : back_graphs) {
    graph->SetLineColor(kBlue - 4);
    graph->Draw("same L");
  }

  //////////////////////////////////////////
  // create the second axis on the rhs
  // auto rhs_axis_color = kOrange + 6;
  auto rhs_axis_color = kTeal - 7;

  TPad* clear_pad = new TPad("clear_pad", "", 0, 0, 1, 1);
  clear_pad->SetFillColor(0);
  clear_pad->SetFillStyle(4000);
  clear_pad->SetFrameFillStyle(0);
  clear_pad->Draw();
  clear_pad->cd();
  clear_pad->SetTicks(0, 0);

  auto axishist_2 = new TH1F("", "", 10, axis_hist->GetXaxis()->GetXmin(),
                             axis_hist->GetXaxis()->GetXmax());
  axishist_2->SetAxisRange(min_co2, max_co2, "Y");
  axishist_2->SetLabelOffset(999);
  axishist_2->SetLabelSize(0);

  auto xaxis2 = axishist_2->GetXaxis();
  xaxis2->SetLimits(data.time.at(0), data.time.at(data.time.size() - 1));
  xaxis2->SetTimeDisplay(1);
  xaxis2->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  xaxis2->SetTimeOffset(0);

  auto yaxis2 = axishist_2->GetYaxis();
  yaxis2->SetLabelFont(42);
  yaxis2->SetAxisColor(rhs_axis_color);
  yaxis2->SetLabelColor(rhs_axis_color);
  yaxis2->SetTitle("CO_{2} flow [g/s]");
  yaxis2->SetTitleColor(rhs_axis_color);
  axishist_2->Draw("Y+");

  //////////////////////////////////////////

  // draw the co2 flow
  co2_flow_graph->SetLineColor(rhs_axis_color);
  co2_flow_graph->SetLineStyle(2);
  co2_flow_graph->SetLineWidth(2);
  co2_flow_graph->Draw("same L");

  canvas->cd();
  gPad->RedrawAxis();

  // add legend
  auto legend = new TLegend(0.6, 0.9, 0.8, 0.75);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.05);
  legend->AddEntry(front_graphs.at(0), "PT100 front", "l");
  legend->AddEntry(back_graphs.at(0), "PT100 back", "l");
  legend->AddEntry(co2_flow_graph, "CO_{2} flow", "l");
  legend->Draw("same");

  AL::myText(0.13, 0.96, kBlack, ("Run name: " + data.run_name).Data(), 0.035);

  TString plot_path = data.config.getStr("output_plot_path", "default");
  TString plot_name = Form("%s/plotAllPT100AndCO2Flow_%s.pdf", plot_path.Data(),
                           data.run_name.Data());

  // cout << "canvas->GetWindowWidth() " << canvas->GetWindowWidth() << '\n';
  // cout << "canvas->GetWindowHeight() " << canvas->GetWindowHeight() << '\n';

  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void plotAllPT100AndCO2FlowAndPower(RunData& data) {
  // get graph for the front and back PT100s and get the CO2 flow graph
  vector<TGraph*> front_graphs;
  vector<TGraph*> back_graphs;
  TGraph* co2_flow_graph = nullptr;
  // vector<TGraph*> power_graphs;
  TGraph* total_power_graphs = nullptr;
  TGraph* inlet_temp_graph = nullptr;

  float min_temp = 0;
  float max_temp = 0;
  float min_co2 = 0;
  float max_co2 = 0;
  float min_pow = 0;
  float max_pow = 0;

  for (auto& sensor : data.sensors) {
    // create the graph
    auto* graph = sensor.createValVsTimeGraph(true);
    if (sensor.name.Contains("BT")) {
      back_graphs.push_back(graph);
      updateMinMaxVals(min_temp, max_temp, sensor);
    } else if (sensor.name.Contains("FT")) {
      front_graphs.push_back(graph);
      updateMinMaxVals(min_temp, max_temp, sensor);
    } else if (sensor.name == "CO2flow") {
      co2_flow_graph = graph;
      updateMinMaxVals(min_co2, max_co2, sensor);
    } else if (sensor.name.Contains("CPD")) {
      inlet_temp_graph = graph;
    } else if (sensor.name.Contains("TotalPower")) {
      // cout << "sensor.name " << sensor.name << '\n';
      // power_graphs.push_back(graph);
      delete graph;
      total_power_graphs = sensor.createValVsTimeGraphFloorNan();
      updateMinMaxVals(min_pow, max_pow, sensor);
      // cout << "pow min: " << min_pow << " pow max: " << max_pow << '\n';
    } else {
      delete graph;
    }
  }

  // setup the canvas
  AL::StyleSetter setter;
  setter.paperx = 100;
  setter.papery = 20;
  setter.top_margin = 0.05;
  setter.right_margin = 0.22;
  setter.bottom_margin = 0.16;
  setter.left_margin = 0.1;
  setter.title_x_offset = 1.4;
  setter.title_y_offset = 0.75;
  setter.label_x_offset = 0.03;
  setter.label_y_offset = 0.01;
  setter.font_size = 0.05;
  setter.set_ticks_x = 0;
  setter.set_ticks_y = 0;

  setVariableStyle(setter);

  // auto canvas = new TCanvas(); // is 700x500 by default
  auto canvas = new TCanvas("canvas", "canvas", 1000, 500);

  auto axis_hist = new TH1F("", ";;Temperature [#circC]", 1, 0, 1);
  axis_hist->SetAxisRange(min_temp - 2, max_temp + 2, "Y");
  auto xaxis = axis_hist->GetXaxis();
  auto yaxis = axis_hist->GetYaxis();
  xaxis->SetTimeDisplay(1);
  xaxis->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  // xaxis->SetTimeFormat("%H:%M");
  xaxis->SetTimeOffset(0);
  // xaxis->SetNdivisions(-219);
  xaxis->SetLimits(data.time.at(0), data.time.at(data.time.size() - 1));
  // xaxis->CenterLabels();
  axis_hist->Draw("*");

  // draw the graphs
  for (const auto& graph : front_graphs) {
    graph->SetLineColor(kMagenta - 4);
    graph->Draw("same L");
  }
  for (const auto& graph : back_graphs) {
    graph->SetLineColor(kBlue - 4);
    graph->Draw("same L");
  }

  inlet_temp_graph->SetLineColor(kRed);
  inlet_temp_graph->Draw("same L");

  //////////////////////////////////////////
  // create the second axis on the rhs
  // auto rhs_axis_color = kOrange + 6;
  auto rhs_axis_color = kTeal - 7;

  TPad* clear_pad = new TPad("clear_pad", "", 0, 0, 1, 1);
  clear_pad->SetFillColor(0);
  clear_pad->SetFillStyle(4000);
  clear_pad->SetFrameFillStyle(0);
  clear_pad->Draw();
  clear_pad->cd();
  clear_pad->SetTicks(0, 0);

  auto axishist_2 = new TH1F("", "", 10, axis_hist->GetXaxis()->GetXmin(),
                             axis_hist->GetXaxis()->GetXmax());
  axishist_2->SetAxisRange(min_co2, max_co2, "Y");
  axishist_2->SetLabelOffset(999);
  axishist_2->SetLabelSize(0);

  auto xaxis2 = axishist_2->GetXaxis();
  xaxis2->SetLimits(data.time.at(0), data.time.at(data.time.size() - 1));
  xaxis2->SetTimeDisplay(1);
  xaxis2->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  xaxis2->SetTimeOffset(0);
  xaxis2->SetLabelSize(0);
  xaxis2->SetLabelSize(0);
  xaxis2->SetTickLength(0);
  xaxis2->SetTickLength(0);

  auto yaxis2 = axishist_2->GetYaxis();
  yaxis2->SetLabelFont(42);
  yaxis2->SetAxisColor(rhs_axis_color);
  yaxis2->SetLabelColor(rhs_axis_color);
  yaxis2->SetTitle("CO_{2} flow [g/s]");
  yaxis2->SetTitleColor(rhs_axis_color);
  axishist_2->Draw("*Y+");

  // draw the co2 flow
  co2_flow_graph->SetLineColor(rhs_axis_color);
  co2_flow_graph->SetLineStyle(2);
  co2_flow_graph->SetLineWidth(2);
  co2_flow_graph->Draw("same L");

  //////////////////////////////////////////

  // cout << "pow min: " << min_pow << " pow max: " << max_pow << '\n';
  if (max_pow == 0) {
    max_pow = 1;
  } else {
    //add 10% for nicer plotting
    max_pow = 1.1 * max_pow;
  }

  auto power_color = kOrange + 7;

  TPad* clear_pad2 = new TPad("clear_pad2", "", 0, 0, 1, 1);
  clear_pad2->SetFillColor(0);
  clear_pad2->SetFillStyle(4000);
  clear_pad2->SetFrameFillStyle(0);
  clear_pad2->Draw();
  clear_pad2->cd();
  clear_pad2->SetTicks(0, 0);

  auto axishist_3 =
      new TH1F("axishist_3", "", 10, axis_hist->GetXaxis()->GetXmin(),
               axis_hist->GetXaxis()->GetXmax());
  axishist_3->SetAxisRange(min_pow, max_pow, "Y");
  axishist_3->SetLabelOffset(999);
  axishist_3->SetLabelSize(0);

  auto xaxis3 = axishist_3->GetXaxis();
  xaxis3->SetLimits(data.time.at(0), data.time.at(data.time.size() - 1));
  xaxis3->SetTimeDisplay(1);
  xaxis3->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  xaxis3->SetTimeOffset(0);
  xaxis3->SetLabelSize(0);
  xaxis3->SetLabelSize(0);
  xaxis3->SetTickLength(0);
  xaxis3->SetTickLength(0);

  auto yaxis3 = axishist_3->GetYaxis();
  yaxis3->SetLabelFont(42);
  // yaxis3->SetAxisColor(power_color);
  // yaxis3->SetLabelColor(power_color);
  // // yaxis3->SetTitle("Power [W]");
  // yaxis3->SetTitleColor(power_color);
  yaxis3->SetLabelSize(0);
  yaxis3->SetLabelSize(0);
  yaxis3->SetTickLength(0);
  yaxis3->SetTickLength(0);
  axishist_3->Draw("*same");

  // TGaxis::TGaxis(Double_t xmin, Double_t ymin, Double_t xmax, Double_t ymax,
  //              Double_t wmin, Double_t wmax, Int_t ndiv, Option_t *chopt,
  //              Double_t gridlength)
  // xmin : X origin coordinate in user's coordinates space.
  // xmax : X end axis coordinate in user's coordinates space.
  // ymin : Y origin coordinate in user's coordinates space.
  // ymax : Y end axis coordinate in user's coordinates space.
  // wmin : Lowest value for the tick mark labels written on the axis.
  // wmax : Highest value for the tick mark labels written on the axis.
  // add an axis, take values from 3rd y axis
  // cout << "xaxis->GetXmin() " << xaxis->GetXmin() << '\n';
  // cout << "xaxis->GetXmax() " << xaxis->GetXmax() << '\n';
  // cout << "yaxis->GetXmin() " << yaxis->GetXmin() << '\n';
  // cout << "yaxis->GetXmax() " << yaxis->GetXmax() << '\n';
  // cout << "yaxis3->GetXmin() " << yaxis3->GetXmin() << '\n';
  // cout << "yaxis3->GetXmax() " << yaxis3->GetXmax() << '\n';
  // cout << "yaxis->GetXmin() " << yaxis->GetXmin() << '\n';
  // cout << "XXXX " << XXXX << '\n';
  // cout << "XXXX " << XXXX << '\n';
  // auto gaxis3 = new TGaxis(xaxis->GetXmax(), yaxis->GetXmin(),
  // xaxis->GetXmax(),
  //                          yaxis->GetXmax(), yaxis3->GetXmin(),
  //                          yaxis3->GetXmax(), 510, "+L");
  auto offset = 0.19 * (xaxis->GetXmax() - xaxis->GetXmin());
  auto gaxis3 =
      new TGaxis(xaxis->GetXmax() + offset, min_pow, xaxis->GetXmax() + offset,
                 max_pow, min_pow, max_pow, 510, "+L");
  gaxis3->SetTitleFont(42);
  gaxis3->SetTitleSize(0.05);
  gaxis3->SetLabelFont(42);
  gaxis3->SetLabelSize(0.05);
  gaxis3->SetLabelColor(power_color);
  gaxis3->SetLineColor(power_color);
  gaxis3->SetTitleOffset(0.7);
  gaxis3->SetTitle("Power [W]");
  gaxis3->SetTitleColor(power_color);
  gaxis3->Draw();

  total_power_graphs->SetLineColor(power_color);
  total_power_graphs->SetLineWidth(2);
  total_power_graphs->SetLineStyle(3);
  total_power_graphs->Draw("same L");

  // for (const auto& graph : power_graphs) {
  //   graph->SetLineColor(power_color);
  //   graph->SetLineWidth(2);
  //   graph->SetLineStyle(3);
  //   graph->Draw("same L");
  // }

  //////////////////////////////////////////

  canvas->cd();
  gPad->RedrawAxis();

  // add legend
  auto legend = new TLegend(0.32, 0.94, 0.59, 0.76);
  legend->SetNColumns(2);
  legend->SetMargin(0.15);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.04);
  legend->AddEntry(front_graphs.at(0), "PT100 front", "l");
  legend->AddEntry(back_graphs.at(0), "PT100 back", "l");
  legend->AddEntry(inlet_temp_graph, "PT100 inlet", "l");
  legend->AddEntry(co2_flow_graph, "CO_{2} flow", "l");
  // legend->AddEntry(power_graphs.at(0), "Power", "l");
  legend->AddEntry(total_power_graphs, "Total power", "l");

  legend->Draw("same");

  TString text = "Run name: " + data.run_name + ", Configuration: " +
                 data.config.getStr("setup_info", "unknown");

  AL::myText(0.1, 0.96, kBlack, text.Data(), 0.035);

  TString plot_path = data.config.getStr("output_plot_path", "default");
  TString plot_name = Form("%s/plotAllPT100AndCO2FlowAndPower_%s.pdf",
                           plot_path.Data(), data.run_name.Data());

  // cout << "canvas->GetWindowWidth() " << canvas->GetWindowWidth() << '\n';
  // cout << "canvas->GetWindowHeight() " << canvas->GetWindowHeight() << '\n';

  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void combinationPlots() {

  // gErrorIgnoreLevel = kWarning;

  RunData data_v2r1 =
      RunDataBuilder::retrieveRunData("globalConfig.cfg", "configV2R1.cfg");
  // RunData data_v2t1 =
  //     RunDataBuilder::retrieveRunData("globalConfig.cfg", "configV2T1.cfg");

  // plotAllPT100AndCO2Flow(data_v2r1);
  plotAllPT100AndCO2FlowAndPower(data_v2r1);
  // plotAllPT100AndCO2FlowAndPower(data_v2t1);
}
