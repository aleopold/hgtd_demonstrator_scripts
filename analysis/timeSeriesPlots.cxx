#include "Configurator.h"
#include "PlottingStyle.h"
#include "PlottingUtils.h"
#include "TError.h"

#include "sensorObjectAndReader.cxx"

void plotAllSensorsTimeAxis(RunData* run_data) {

  AL::setTimeAxisStyle();

  // get datetime vector as floats
  std::vector<float> time_stamps = run_data->time;

  // auto g = new TGraph(values->size(), timeStamps->data(), values->data());

  auto canvas = new TCanvas();

  auto axis_hist =
      new TH1F("axis_hist", ";Time; Temperature [#circC]", 10, 0, 1);
  auto xaxis = axis_hist->GetXaxis();
  xaxis->SetTimeDisplay(1);
  // xaxis->CenterTitle();
  xaxis->SetTimeFormat("#splitline{%d/%m}{%H:%M}");
  // xaxis->SetTimeFormat("%H:%M");
  xaxis->SetTimeOffset(0);
  // xaxis->SetNdivisions(-219);
  xaxis->SetLimits(time_stamps.at(0), time_stamps.at(time_stamps.size() - 1));
  // xaxis->SetLabelSize(0.025);
  xaxis->CenterLabels();
  axis_hist->Draw();

  TString plot_path = run_data->config->getStr("output_plot_path", "default");
  TString plot_name = Form("%s/plotAllSensorsTimeAxis_%s.pdf", plot_path.Data(),
                           run_data->run_name.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

void plotAllSensors(RunData* run_data, bool set_time_range) {

  auto legend_front = new TLegend(0.6, 0.5, 0.7, 0.9);
  auto legend_back = new TLegend(0.7, 0.5, 0.9, 0.9);
  // legend->SetNColumns(2);
  legend_front->SetTextFont(42);
  legend_front->SetFillStyle(0);
  legend_front->SetBorderSize(0);
  legend_front->SetTextSize(0.03);
  legend_back->SetTextFont(42);
  legend_back->SetFillStyle(0);
  legend_back->SetBorderSize(0);
  legend_back->SetTextSize(0.03);

  int n_meas_max = 0;
  // draw each graph
  Color_t front_color = kPink - 9;
  Color_t back_color = kAzure - 9;
  bool first = true;
  int color_i = 0;
  vector<TGraph*> graphs;
  for (auto& sensor : run_data->sensors) {

    if (sensor.name.Contains("CO2")) {
      continue;
    }

    TGraph* graph = nullptr;
    if (set_time_range) {
      graph = sensor.createTempVsReadingGraph(
          TDatime(run_data->config->getStr("time_start", "")),
          TDatime(run_data->config->getStr("time_end", "")));
    } else {
      graph = sensor.createTempVsReadingGraph();
    }
    graph->SetName(sensor.short_name);
    graph->SetLineWidth(1);

    if (sensor.name.Contains("F")) {
      graph->SetLineColor(front_color + color_i);
      legend_front->AddEntry(graph, sensor.short_name, "l");
    } else if (sensor.name.Contains("B")) {
      graph->SetLineColor(back_color + color_i);
      legend_back->AddEntry(graph, sensor.short_name, "l");
    } else {
      graph->SetLineColor(kBlack);
      graph->SetLineStyle(2);
    }
    graphs.push_back(graph);
    n_meas_max = std::max(n_meas_max, graph->GetN());
    color_i++;
  }

  auto canvas = new TCanvas();
  auto axis_hist = new TH1F("axis_hist", ";Measurement; Temperature [#circC]",
                            100, 0, n_meas_max);
  axis_hist->SetAxisRange(-44, 25, "Y");
  axis_hist->Draw();

  for (const auto& graph : graphs) {
    graph->Draw("same L");
  }

  legend_front->Draw("same");
  legend_back->Draw("same");

  AL::myText(0.2, 0.88, kBlack, "Setup:", 0.04, 0.);
  AL::myText(0.2, 0.83, kBlack,
             run_data->config->getStr("setup_info", "default"), 0.04, 0.);

  TString plot_path = run_data->config->getStr("output_plot_path", "default");
  TString plot_name = Form("%s/plotAllSensors_%s_%i.pdf", plot_path.Data(),
                           run_data->run_name.Data(), set_time_range);
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

void timeSeriesPlots(/*TString config_name = "configPlotting.cfg"*/) {

  gErrorIgnoreLevel = kWarning;

  AL::Configurator global_config("globalConfig.cfg");

  cout << "Load global config \n";

  // AL::Configurator config_steel("configSteelPlotting.cfg");
  // config_steel.setValue("output_plot_path",
  //                       global_config.getStr("output_plot_path"));
  //
  // AL::Configurator config_ti("configTiPlotting.cfg");
  // config_ti.setValue("output_plot_path",
  //                    global_config.getStr("output_plot_path"));
  // AL::Configurator config_ti2("configTi2Plotting.cfg");
  // config_ti2.setValue("output_plot_path",
  //                     global_config.getStr("output_plot_path"));
  // AL::Configurator config_tial("configTiAlPlotting.cfg");
  // config_tial.setValue("output_plot_path",
  //                      global_config.getStr("output_plot_path"));
  // AL::Configurator config_tial2("configTiAl2Plotting.cfg");
  // config_tial2.setValue("output_plot_path",
  //                       global_config.getStr("output_plot_path"));
  AL::Configurator config_v2r1("configV2R1.cfg");
  config_v2r1.setValue("output_plot_path",
                       global_config.getStr("output_plot_path"));

  cout << "Done with Configurators \n";

  // TFile* file_steel =
  //     TFile::Open(config_steel.getStr("input_root_file", "default"), "READ");
  // TFile* file_ti =
  //     TFile::Open(config_ti.getStr("input_root_file", "default"), "READ");
  // TFile* file_ti2 =
  //     TFile::Open(config_ti2.getStr("input_root_file", "default"), "READ");
  // TFile* file_tial =
  //     TFile::Open(config_tial.getStr("input_root_file", "default"), "READ");
  // TFile* file_tial2 =
  //     TFile::Open(config_tial2.getStr("input_root_file", "default"), "READ");
  TFile* file_v2r1 =
      TFile::Open(config_v2r1.getStr("input_root_file", "default"), "READ");

  cout << "Done opening the rootfiles \n";

  AL::preparePlottingDir(global_config, "output_plot_path");

  // RunData data_steel = RunDataBuilder::readRunData(file_steel,
  // &config_steel); RunData data_ti = RunDataBuilder::readRunData(file_ti,
  // &config_ti); RunData data_ti2 = RunDataBuilder::readRunData(file_ti2,
  // &config_ti2); RunData data_tial = RunDataBuilder::readRunData(file_tial,
  // &config_tial); RunData data_tial2 = RunDataBuilder::readRunData(file_tial2,
  // &config_tial2);
  RunData data_v2r1 = RunDataBuilder::readRunData(file_v2r1, &config_v2r1);

  // data_tial.getSensor("CPD")->printStartAndEnd();
  // return;

  AL::setAtlasStyle();

  plotAllSensors(&data_v2r1, false);

  plotAllSensorsTimeAxis(&data_v2r1);
}
