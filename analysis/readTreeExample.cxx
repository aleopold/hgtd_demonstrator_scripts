

void mainFunc(TEnv& config) {
  // open rootfile
  TString input_xlsx_file = config.GetValue("input_xlsx_file", "TestFile.xlsx");
  TString rootfile_name = input_xlsx_file.ReplaceAll(".xlsx", ".root");

  std::cout << "reading " << rootfile_name << "...\n";

  // auto file = TFile::Open(rootfile_name, "READ");
  // auto tree = (TTree*)file->Get("tree");

  TFile file(rootfile_name, "READ");
  TTreeReader reader("tree", &file);
  // TTreeReaderValue<TTimeStamp> date_time(reader, "date_time");
  TTreeReaderValue<TDatime> date_time(reader, "date_time");

  std::vector<TTreeReaderValue<float>*> readers;
  TTreeReaderValue<float> temp_F2_101(reader, "F2_101");


  std::vector<float> time;
  std::vector<float> temp;

  int n = reader.GetEntries();
  time.reserve(n);
  temp.reserve(n);

  while (reader.Next()) {
    // date_time->Print();
    time.push_back((*date_time).Convert());
    temp.push_back(*temp_F2_101);
    std::cout << *temp_F2_101 << '\n';
  }

  auto* mgr = new TGraph(n, &time[0], &temp[0]);
  mgr->SetMarkerStyle(20);

  mgr->Draw("ap");
  mgr->GetXaxis()->SetTimeDisplay(1);
  mgr->GetXaxis()->SetNdivisions(503);
  mgr->GetXaxis()->SetTimeFormat("%Y-%m-%d %H:%M");
  mgr->GetXaxis()->SetTimeOffset(0, "gmt");

}

void readTreeExample(TString config_name = "configXLS2ROOT.cfg") {

  TEnv config = TEnv(config_name);

  mainFunc(config);
}
