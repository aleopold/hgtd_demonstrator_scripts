#include "Configurator.h"
#include "PlottingStyle.h"
#include "PlottingUtils.h"

#include "sensorObjectAndReader.cxx"

void plot(TString sensor_id, TString y_label, float ymin, float ymax,
          std::vector<RunData*> data_vec, bool set_time_range) {

  vector<TGraph*> graphs;
  size_t n_meas_max = 0;
  std::vector<Color_t> cols = {30, 40, 42, 46, 38, 49};
  int col_i = 0;
  for (auto& data : data_vec) {
    auto* sensor = data->getSensor(sensor_id);
    TGraph* graph = nullptr;
    if (set_time_range) {
      graph = sensor->createTempVsReadingGraph(
          TDatime(data->config->getStr("time_start", "")),
          TDatime(data->config->getStr("time_end", "")));
    } else {
      graph = sensor->createTempVsReadingGraph();
    }

    graph->SetName(data->run_name);
    graph->SetLineColor(cols.at(col_i));
    col_i++;
    graph->SetLineWidth(2);
    graphs.push_back(graph);
    n_meas_max = std::max(n_meas_max, sensor->data.size());
  }

  auto canvas = new TCanvas();

  auto axis_hist = new TH1F("axis_hist", "", 100, 0, n_meas_max);
  axis_hist->SetAxisRange(ymin, ymax, "Y");
  axis_hist->Draw();
  axis_hist->GetYaxis()->SetTitle(y_label);
  axis_hist->GetXaxis()->SetTitle("Measurement");

  for (auto& graph : graphs) {
    graph->Draw("same L");
  }

  auto legend = new TLegend(0.75, 0.8, 0.85, 0.65);
  // legend->SetNColumns(2);
  legend->SetTextFont(42);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.03);
  for (auto& graph : graphs) {
    legend->AddEntry(graph, graph->GetName(), "l");
  }
  legend->Draw("same");

  AL::myText(0.7, 0.85, kBlack, Form("Probe ID: %s", sensor_id.Data()), 0.04,
                0.);

  // for this I can take any of the configs
  TString plot_path =
      data_vec.at(0)->config->getStr("output_plot_path", "default");
  TString plot_name = Form("%s/overlaySensorReadings_%i_%s.pdf",
                           plot_path.Data(), set_time_range, sensor_id.Data());
  canvas->Print(plot_name);
  plot_name.ReplaceAll(".pdf", ".png");
  canvas->Print(plot_name);
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// void plotInletTemp(TempSensor* sensor, TEnv& config, TString run) {
//   auto canvas = new TCanvas();
//   sensor->graph->Draw("AL");
//   sensor->graph->GetYaxis()->SetTitle("Temperature [#circ C]");
//   sensor->graph->GetXaxis()->SetTitle("Time");
//   sensor->graph->SetLineWidth(4);
//   sensor->graph->GetXaxis()->SetTimeDisplay(1);
//   sensor->graph->GetXaxis()->SetNdivisions(503);
//   sensor->graph->GetXaxis()->SetTimeFormat("%d-%m %H:%M");
//
//   atlas::myText(0.2, 0.88, kBlack, "Setup:", 0.04, 0.);
//   atlas::myText(0.28, 0.88, kBlack, config.getStr("setup_info", "default"),
//                 0.04, 0.);
//
//   TString plot_path = config.getStr("output_plot_path", "default");
//   TString plot_name =
//       Form("%s/overlaySensorReadings_%s.pdf", plot_path.Data(), run.Data());
//   canvas->Print(plot_name);
//   plot_name.ReplaceAll(".pdf", ".png");
//   canvas->Print(plot_name);
// }

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void overlaySensorReadings() {

  AL::Configurator global_config("globalConfig.cfg");

  // AL::Configurator config_steel("configSteelPlotting.cfg");
  // config_steel.setValue("output_plot_path",
  //                       global_config.getStr("output_plot_path"));
  // AL::Configurator config_ti("configTiPlotting.cfg");
  // config_ti.setValue("output_plot_path",
  //                    global_config.getStr("output_plot_path"));
  // AL::Configurator config_ti2("configTi2Plotting.cfg");
  // config_ti2.setValue("output_plot_path",
  //                     global_config.getStr("output_plot_path"));
  // AL::Configurator config_tial("configTiAlPlotting.cfg");
  // config_tial.setValue("output_plot_path",
  //                      global_config.getStr("output_plot_path"));
  // AL::Configurator config_tial2("configTiAl2Plotting.cfg");
  // config_tial2.setValue("output_plot_path",
  //                       global_config.getStr("output_plot_path"));
   AL::Configurator config_v2r1("configV2R1.cfg");
   config_v2r1.setValue("output_plot_path",
                         global_config.getStr("output_plot_path"));

  // TFile* file_steel =
  //     TFile::Open(config_steel.getStr("input_root_file", "default"), "READ");
  // TFile* file_ti =
  //     TFile::Open(config_ti.getStr("input_root_file", "default"), "READ");
  // TFile* file_ti2 =
  //     TFile::Open(config_ti2.getStr("input_root_file", "default"), "READ");
  // TFile* file_tial =
  //     TFile::Open(config_tial.getStr("input_root_file", "default"), "READ");
  // TFile* file_tial2 =
  //     TFile::Open(config_tial2.getStr("input_root_file", "default"), "READ");
  TFile* file_v2r1 =
      TFile::Open(config_v2r1.getStr("input_root_file", "default"), "READ");

  AL::preparePlottingDir(config_v2r1, "output_plot_path");

  // RunData data_steel = RunDataBuilder::readRunData(file_steel, &config_steel);
  // RunData data_ti = RunDataBuilder::readRunData(file_ti, &config_ti);
  // RunData data_ti2 = RunDataBuilder::readRunData(file_ti2, &config_ti2);
  // RunData data_tial = RunDataBuilder::readRunData(file_tial, &config_tial);
  // RunData data_tial2 = RunDataBuilder::readRunData(file_tial2, &config_tial2);
  RunData data_v2r1 = RunDataBuilder::readRunData(file_v2r1, &config_v2r1);

  AL::setAtlasStyle();

  plot("CPD", "Temperature [#circ C]", -44, -36,
       {&data_v2r1}, false);
  plot("CPD", "Temperature [#circ C]", -44, -36,
       {&data_v2r1}, true);

  plot("CO2flow", "CO_{2} flow [g/s]", 0, 10,
       {&data_v2r1}, false);
  plot("CO2flow", "CO_{2} flow [g/s]", 0, 10,
       {&data_v2r1}, true);

  plot("CO2Tin", "Baby-demo inlet temperature [#circ C]", -40, -25,
       {&data_v2r1}, false);
  plot("CO2Tin", "Baby-demo inlet temperature [#circ C]", -40, -25,
       {&data_v2r1}, true);

  plot("CO2Tout", "Baby-demo outlet temperature [#circ C]", -40, -25,
       {&data_v2r1}, false);
  plot("CO2Tout", "Baby-demo outlet temperature [#circ C]", -40, -25,
       {&data_v2r1}, true);
}
