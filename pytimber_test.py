from datetime import datetime

def main():
    import pytimber
    """
    Testing variables:
    CS1B_180_TT2H02.POSST  - HGTD outlet temperature - Rack 2 Card 4 ch 7
    CS1B_180_TT1H01.POSST  - HGTD inlet temperature - Rack 2 Card 4 ch 6
    CS1B_180_PT2H02.POSST  - HGTD outlet pressure - Rack 2 Card 5 ch 1
    CS1B_180_PT1H01..POSST - HGTD inlet pressure - Rack 2 Card 5 ch 0
    CS1B_180_FT1H01.POSST  - HGTD flow measurement - Rack 2 Card 4 ch 5
    """
    ldb = pytimber.LoggingDB(source="nxcals")
    # t1 = '2021-02-17 20:00:00.000'
    # t2 = '2021-02-18 09:00:00.000'
    ##Start time: 2021-05-12 10:56:24.171000, end time: 2021-05-12 17:13:34.172000
    t1 = '2021-05-12 10:56:24'
    t2 = '2021-05-12 20:13:34'
    # vars = ['CS1B_180_TT2H02.POSST', 'CS1B_180_TT1H01.POSST', 'CS1B_180_PT2H02.POSST', 'CS1B_180_PT1H01.POSST', 'CS1B_180_FT1H01.POSST']
    vars = ['CS1B_180_FT1H01.POSST']

    # f = open("~/www/tmp/co2flow.txt", "w")
    for var in vars:
        d = ldb.get(var, t1, t2)
        if not var in d:
            print(var, "not found")
            continue
        ## d is a dict, time and vals in tuple that holds 2 arrays, time is
        ## a UTC timestamp, can be transformed via e.g.
        ## datetime.utcfromtimestamp(timestamp)
        ## NOTE: UTC is 'CERN time - 2h'

        # utc = d[var][0][0]
        # print(datetime.utcfromtimestamp(utc))
        # print(var, ": ", d[var][1][0])
        # print(d[var][1])
        # for t, val in zip(d[var][0], d[var][1]):
        #     print
        for d, val in zip(d[var][0][:], d[var][1][:]):
            print(f'{datetime.utcfromtimestamp(d)}, {val}')
            # f.write(f'{datetime.utcfromtimestamp(utc)}, {}')
    # f.close()



if __name__ == "__main__":
    main()
